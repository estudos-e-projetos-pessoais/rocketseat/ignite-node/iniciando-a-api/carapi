module.exports = {
  env: {
    es2020: true,
    node: true,
    jest: true,
  },
  globals: {
    ENV: true,
    it: true,
  },
  extends: [
    'eslint:recommended',
    'airbnb-base',
    'plugin:@typescript-eslint/recommended',
  ],
  settings: {
    'import/resolver': {
      node: {
        extensions: ['.js', '.jsx', '.ts', '.tsx'],
      },
    },
  },
  parser: '@typescript-eslint/parser',
  plugins: [
    '@typescript-eslint',
    'eslint-plugin-import-helpers',
  ],
  rules: {
    'import/no-unresolved': 'error',
    'import/extensions': [
      'error',
      'ignorePackages',
      {
        js: 'never',
        ts: 'never',
      },
    ],
    'import/prefer-default-export': 'off',
    'import-helpers/order-imports': [
      'warn',
      {
        newlinesBetween: 'always',
        groups: ['module', '/^@shared/', ['index', 'sibling', 'parent']],
        alphabetize: { order: 'asc', ignoreCase: true },
      },
    ],
    'import/no-extraneous-dependencies': [
      'error',
      { devDependencies: ['**/*.spec.js'] },
    ],
    'no-useless-constructor': 0,
    '@typescript-eslint/naming-convention': [
      'error',
      {
        selector: 'interface',
        format: ['PascalCase'],
        custom: {
          regex: '[A-Z]*Interface',
          match: true,
        },
      },
    ],
    'class-methods-use-this': 0,
    indent: ['error', 2], // 2 spaces indentation
    'linebreak-style': ['error', 'unix'], // \n instead of \r\n
    quotes: ['error', 'single'], // single quotes preferred
    semi: ['error', 'always'], // always use semicolons
    'brace-style': ['error', 'stroustrup', { allowSingleLine: true }], // Overrides Airbnb: statements on separated lines
    'max-len': ['error', {
      code: 120,
      tabWidth: 2,
      ignoreUrls: true,
      ignoreStrings: true,
      ignoreComments: true,
      ignoreTemplateLiterals: true,
      ignoreRegExpLiterals: true,
    }],
    'no-underscore-dangle': 0,
    'no-console': ['warn', { allow: ['warn', 'error'] }],
    camelcase: 0,
  },
};
