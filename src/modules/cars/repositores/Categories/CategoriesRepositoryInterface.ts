import Category from '../../models/Category';

export interface CreateCategoryDTOInterface {
  name: string;
  description: string;
}

interface CategoriesRepositoryInterface {
  findByName(name: string): Promise<Category>,
  index(): Promise<Category[]>,
  create({ name, description }: CreateCategoryDTOInterface): Promise<Category>,
}

export default CategoriesRepositoryInterface;
