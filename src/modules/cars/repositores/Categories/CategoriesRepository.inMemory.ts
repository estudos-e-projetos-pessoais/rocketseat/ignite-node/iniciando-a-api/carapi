import Category from '../../models/Category';
import CategoriesRepositoryInterface, { CreateCategoryDTOInterface } from './CategoriesRepositoryInterface';

class CategoriesRepositoryInMemory implements CategoriesRepositoryInterface {
  categories: Category[] = [];

  async index(): Promise<Category[]> {
    return this.categories;
  }

  async create({ name, description }: CreateCategoryDTOInterface): Promise<Category> {
    const newCategory = new Category();

    Object.assign(newCategory, {
      name,
      description,
      created_at: Date.now(),
    });

    this.categories = [...this.categories, newCategory];

    return newCategory;
  }

  async findByName(name: string): Promise<Category> {
    return this.categories.find((d) => d.name === name);
  }
}

export default CategoriesRepositoryInMemory;
