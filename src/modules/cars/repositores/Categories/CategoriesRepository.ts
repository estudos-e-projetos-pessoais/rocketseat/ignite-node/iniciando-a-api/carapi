import { getRepository, Repository } from 'typeorm';

import Category from '../../models/Category';
import CategoriesRepositoryInterface, { CreateCategoryDTOInterface } from './CategoriesRepositoryInterface';

class CategoriesRepository implements CategoriesRepositoryInterface {
  private repository: Repository<Category>;

  constructor() {
    this.repository = getRepository(Category);
  }

  index(): Promise<Category[]> {
    return this.repository.find();
  }

  create({ name, description }: CreateCategoryDTOInterface): Promise<Category> {
    const newCategory = this.repository.create({
      name,
      description,
    });

    return this.repository.save(newCategory);
  }

  findByName(name: string): Promise<Category> {
    return this.repository.findOne({ name });
  }
}

export default CategoriesRepository;
