import { Repository, getRepository } from 'typeorm';

import Specification from '../../models/Specification';
import SpecificationsRepositoryInterface, { CreateSpecificationDTOInterface } from './SpecificationsRepositoryInterface';

class SpecificationsRepository implements SpecificationsRepositoryInterface {
  private repository: Repository<Specification>;

  constructor() {
    this.repository = getRepository(Specification);
  }

  index(): Promise<Specification[]> {
    return this.repository.find();
  }

  create({ name, description }: CreateSpecificationDTOInterface): Promise<Specification> {
    const newSpecification = this.repository.create({
      name,
      description,
    });

    return this.repository.save(newSpecification);
  }

  async findByName(name: string): Promise<Specification> {
    return this.repository.findOne({ name });
  }
}

export default SpecificationsRepository;
