import Specification from '../../models/Specification';

interface CreateSpecificationDTOInterface {
  name: string;
  description: string;
}

interface SpecificationsRepositoryInterface {
  findByName(name: string): Promise<Specification>,
  index(): Promise<Specification[]>,
  create({ name, description }: CreateSpecificationDTOInterface): Promise<Specification>,
}

export { CreateSpecificationDTOInterface };

export default SpecificationsRepositoryInterface;
