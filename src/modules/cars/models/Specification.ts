import {
  Entity,
  PrimaryColumn,
  Generated,
  Column,
  CreateDateColumn,
} from 'typeorm';
import { v4 as uuid } from 'uuid';

@Entity('specifications')
class Specification {
  @PrimaryColumn({ type: 'uuid', unique: true })
  @Generated('uuid')
  id: string;

  @Column()
  name: string;

  @Column()
  description: string;

  @CreateDateColumn()
  created_at: Date;

  constructor() {
    if (!this.id) {
      this.id = uuid();
    }
  }
}

export default Specification;
