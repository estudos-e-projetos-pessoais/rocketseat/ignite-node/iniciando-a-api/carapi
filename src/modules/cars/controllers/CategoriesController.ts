import { Request, Response } from 'express';
import { container } from 'tsyringe';

import { controllerErrorHandler } from '../../../utils/errorUtils';
import CategoriesService from '../services/Categories/CategoriesService';
import CategoriesServiceInterface from '../services/Categories/CategoriesServiceInterface';

class CategoriesController {
  service: CategoriesServiceInterface;

  constructor() {
    this.service = container.resolve(CategoriesService);
  }

  async index(req: Request, res: Response): Promise<Response> {
    try {
      const categories = await this.service.index();
      return res.json(categories);
    }

    catch (err) {
      return controllerErrorHandler(res, err);
    }
  }

  async create(req: Request, res: Response): Promise<Response> {
    try {
      const newCategory = await this.service.create(req.body);
      return res.status(201).json(newCategory);
    }

    catch (err) {
      return controllerErrorHandler(res, err);
    }
  }

  async import(req: Request, res: Response): Promise<Response> {
    try {
      const newCategories = await this.service.import(req.file);
      return res.status(201).json(newCategories);
    }

    catch (err) {
      return controllerErrorHandler(res, err);
    }
  }
}

export default CategoriesController;
