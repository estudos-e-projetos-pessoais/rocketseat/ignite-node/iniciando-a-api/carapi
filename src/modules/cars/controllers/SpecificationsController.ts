import { Request, Response } from 'express';
import { container } from 'tsyringe';

import { controllerErrorHandler } from '../../../utils/errorUtils';
import SpecificationsService from '../services/Specifications/SpecificationsService';
import SpecificationsServiceInterface from '../services/Specifications/SpecificationsServiceInterface';

class SpecificationsController {
  service: SpecificationsServiceInterface;

  constructor() {
    this.service = container.resolve(SpecificationsService);
  }

  async index(req: Request, res: Response): Promise<Response> {
    try {
      const categories = await this.service.index();
      return res.json(categories);
    }

    catch (err) {
      return controllerErrorHandler(res, err);
    }
  }

  async create(req: Request, res: Response): Promise<Response> {
    try {
      const newSpecification = await this.service.create(req.body);
      return res.status(201).json(newSpecification);
    }

    catch (err) {
      return controllerErrorHandler(res, err);
    }
  }
}

export default SpecificationsController;
