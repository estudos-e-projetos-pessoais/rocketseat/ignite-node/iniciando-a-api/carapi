import { Router } from 'express';
import multer from 'multer';

import CategoriesController from '../controllers/CategoriesController';

const upload = multer({
  dest: './tmp',
});

const categoriesRouter = Router();

const categoriesController = new CategoriesController();

categoriesRouter.get('/',
  categoriesController.index.bind(categoriesController));

categoriesRouter.post('/',
  categoriesController.create.bind(categoriesController));

categoriesRouter.post('/import',
  upload.single('file'),
  categoriesController.import.bind(categoriesController));

export default categoriesRouter;
