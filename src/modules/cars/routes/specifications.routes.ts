import { Router } from 'express';

import { ensureAuthenticatedMiddleware } from '../../../middlewares/ensureAuthenticatedMiddleware';
import SpecificationsController from '../controllers/SpecificationsController';

const specificationsRouter = Router();

const specificationsController = new SpecificationsController();

// Apenas usuários autenticados
specificationsRouter.use(ensureAuthenticatedMiddleware);

specificationsRouter.get('/',
  specificationsController.index.bind(specificationsController));

specificationsRouter.post('/',
  specificationsController.create.bind(specificationsController));

export default specificationsRouter;
