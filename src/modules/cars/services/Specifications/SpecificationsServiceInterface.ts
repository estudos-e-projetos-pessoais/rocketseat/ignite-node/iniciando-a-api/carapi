import Specification from '../../models/Specification';

export interface CreateSpecificationDTOInterface {
  name: string;
  description: string;
}

interface SpecificationsServiceInterface {
  index(): Promise<Specification[]>,
  create({ name, description }: CreateSpecificationDTOInterface): Promise<Specification>,
}

export default SpecificationsServiceInterface;
