import { inject, injectable } from 'tsyringe';

import { ErrorNo } from '../../../../utils/errorUtils';
import Specification from '../../models/Specification';
import SpecificationsRepositoryInterface from '../../repositores/Specifications/SpecificationsRepositoryInterface';
import SpecificationsServicesInterface from './SpecificationsServiceInterface';

interface CreateServiceInterface {
  name: string;
  description: string;
}

@injectable()
class SpecificationsService implements SpecificationsServicesInterface {
  constructor(
    @inject('SpecificationsRepository')
    private repository: SpecificationsRepositoryInterface,
  ) {}

  index(): Promise<Specification[]> {
    return this.repository.index();
  }

  async create({ name, description }: CreateServiceInterface): Promise<Specification> {
    if (await this.repository.findByName(name)) {
      throw new ErrorNo(400, 'Specification already exists');
    }

    return this.repository.create({ name, description });
  }
}

export default SpecificationsService;
