import csvParser from 'csv-parse';
import fs from 'fs';
import { inject, injectable } from 'tsyringe';

import { ErrorNo } from '../../../../utils/errorUtils';
import Category from '../../models/Category';
import CategoriesRepositoryInterface from '../../repositores/Categories/CategoriesRepositoryInterface';
import CategoriesServiceInterface from './CategoriesServiceInterface';

interface CreateCategoryInterface {
  name: string;
  description: string;
}

@injectable()
class CategoriesService implements CategoriesServiceInterface {
  constructor(
    @inject('CategoriesRepository')
    private repository: CategoriesRepositoryInterface,
  ) { }

  index(): Promise<Category[]> {
    return this.repository.index();
  }

  async create({ name, description }: CreateCategoryInterface): Promise<Category> {
    if (await this.repository.findByName(name)) {
      throw new ErrorNo(400, 'Category already exists');
    }

    return this.repository.create({ name, description });
  }

  import(file: Express.Multer.File): Promise<Category[]> {
    return new Promise((resolve, reject) => {
      const readStream = fs.createReadStream(file.path);
      const parseCSV = csvParser({ columns: true });
      let newCategories = [];

      readStream
        .pipe(parseCSV)
        .on('data', async ({ name, description }) => {
          const category = await this.repository.findByName(name);

          if (!category) {
            const newCategory = this.repository.create({ name, description });
            newCategories = [...newCategories, newCategory];
          }
        })
        .on('end', () => {
          fs.promises.unlink(file.path);
          resolve(newCategories);
        })
        .on('error', (err) => reject(err));
    });
  }
}

export default CategoriesService;
