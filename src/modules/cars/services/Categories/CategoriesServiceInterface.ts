import Category from '../../models/Category';

export interface CreateCategoryDTOInterface {
  name: string;
  description: string;
}

interface CategoriesServiceInterface {
  index(): Promise<Category[]>,
  create({ name, description }: CreateCategoryDTOInterface): Promise<Category>,
  import(file: Express.Multer.File): Promise<Category[]>,
}

export default CategoriesServiceInterface;
