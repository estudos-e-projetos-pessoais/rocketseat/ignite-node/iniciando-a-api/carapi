import CategoriesRepositoryInMemory from '../../repositores/Categories/CategoriesRepository.inMemory';
import CategoriesService from './CategoriesService';

let categoriesService: CategoriesService;
let categoriesRepositoryInMemory: CategoriesRepositoryInMemory;

describe('Categories Services', () => {
  beforeEach(() => {
    categoriesRepositoryInMemory = new CategoriesRepositoryInMemory();
    categoriesService = new CategoriesService(categoriesRepositoryInMemory);
  });

  it('should be able to create a new category', async () => {
    const newCategory = await categoriesService.create({
      name: 'Category Test',
      description: 'Category Description Test',
    });

    expect(newCategory)
      .toHaveProperty('id');

    expect(newCategory)
      .toEqual(expect.objectContaining({
        name: 'Category Test',
        description: 'Category Description Test',
      }));
  });

  it('should not be able to create an existing category', () => {
    expect(async () => {
      await categoriesService.create({
        name: 'Category Test',
        description: 'Category Description Test',
      });

      await categoriesService.create({
        name: 'Category Test',
        description: 'Category Description Test',
      });

      console.log(categoriesRepositoryInMemory.index());
    })
      .rejects
      .toEqual(expect.objectContaining({ code: 500 }));
  });
});
