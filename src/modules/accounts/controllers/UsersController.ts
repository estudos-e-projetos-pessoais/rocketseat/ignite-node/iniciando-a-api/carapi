import { Request, Response } from 'express';
import { container } from 'tsyringe';

import { controllerErrorHandler } from '../../../utils/errorUtils';
import UsersService from '../services/Users/UsersService';
import UsersServiceInterface from '../services/Users/UsersServiceInterface';

class UsersController {
  service: UsersServiceInterface;

  constructor() {
    this.service = container.resolve(UsersService);
  }

  async create(req: Request, res: Response): Promise<Response> {
    try {
      const newUser = await this.service.create(req.body);
      return res.status(201).json(newUser);
    }

    catch (err) {
      return controllerErrorHandler(res, err);
    }
  }

  async update(req: Request, res: Response): Promise<Response> {
    try {
      const { id } = req.params;

      const avatar = req.file;

      const user = await this.service.update({
        id,
        ...avatar && { avatar: avatar.filename },
        ...req.body,
      });

      return res.status(200).json(user);
    }

    catch (err) {
      return controllerErrorHandler(res, err);
    }
  }
}

export default UsersController;
