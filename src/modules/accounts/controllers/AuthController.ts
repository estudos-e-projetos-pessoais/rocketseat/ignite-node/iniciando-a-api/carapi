import { Request, Response } from 'express';
import { container } from 'tsyringe';

import { controllerErrorHandler } from '../../../utils/errorUtils';
import AuthService from '../services/Auth/AuthService';
import AuthServiceInterface from '../services/Auth/AuthServiceInterface';

class AuthController {
  service: AuthServiceInterface;

  constructor() {
    this.service = container.resolve(AuthService);
  }

  async authenticate(req: Request, res: Response): Promise<Response> {
    try {
      const { token, user } = await this.service.authenticate(req.body);
      return res.status(201).json({ token, user });
    }

    catch (err) {
      return controllerErrorHandler(res, err);
    }
  }
}

export default AuthController;
