import { hash } from 'bcrypt';
import { inject, injectable } from 'tsyringe';

import { ErrorNo } from '../../../../utils/errorUtils';
import { removeFileFromDisk } from '../../../../utils/file';
import User from '../../models/User';
import UsersRepositoryInterface from '../../repositories/Users/UsersRepositoryInterface';
import UsersServiceInterface, { CreateUserInterface, UpdateUserInterface } from './UsersServiceInterface';

@injectable()
class UsersService implements UsersServiceInterface {
  constructor(
    @inject('UsersRepository')
    private repository: UsersRepositoryInterface,
  ) { }

  // index(): Promise<User[]> {
  //   return this.repository.index();
  // }

  async create({
    name,
    email,
    password,
    driver_license,
    avatar,
  }: CreateUserInterface): Promise<User> {
    if (await this.repository.findByEmail(email)) {
      throw new ErrorNo(400, 'User already exists');
    }

    const passwordHash = await hash(password, 8);

    return this.repository.create({
      name,
      email,
      driver_license,
      avatar,
      password: passwordHash,
    });
  }

  async update({
    id,
    name,
    email,
    driver_license,
    avatar,
  }: UpdateUserInterface): Promise<User> {
    const user = await this.repository.findById(id);

    if (!user) {
      throw new ErrorNo(400, 'User does not exist');
    }

    if (user.avatar) {
      removeFileFromDisk(`./tmp/avatar/${user.avatar}`);
    }

    return this.repository.update({
      id,
      name: name || user.name,
      email: email || user.email,
      driver_license: driver_license || user.driver_license,
      avatar: avatar || user.avatar,
    });
  }
}

export default UsersService;
