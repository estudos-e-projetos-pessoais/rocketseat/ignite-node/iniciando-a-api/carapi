import User from '../../models/User';
import { CreateUserDTOInterface, UpdateUserDTOInterface } from '../../repositories/Users/UsersRepositoryInterface';

export type CreateUserInterface = CreateUserDTOInterface
export type UpdateUserInterface = UpdateUserDTOInterface

interface UsersServiceInterface {
  // index(): Promise<User[]>,
  create(data: CreateUserInterface): Promise<User>,
  update(data: UpdateUserInterface): Promise<User>,
}

export default UsersServiceInterface;
