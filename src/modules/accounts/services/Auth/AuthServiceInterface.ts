export interface CreateSessionInterface {
  email: string;
  password: string;
}

export interface AuthResponseInterface {
  user: {
    name: string;
    email: string;
  },
  token: string;
}

interface AuthServiceInterface {
  authenticate(data: CreateSessionInterface): Promise<AuthResponseInterface>,
}

export default AuthServiceInterface;
