import { compare } from 'bcrypt';
import { sign } from 'jsonwebtoken';
import { inject, injectable } from 'tsyringe';

import { ErrorNo } from '../../../../utils/errorUtils';
import UserRepositoryInterface from '../../repositories/Users/UsersRepositoryInterface';
import AuthServiceInterface, {
  CreateSessionInterface,
  AuthResponseInterface,
} from './AuthServiceInterface';

@injectable()
class AuthService implements AuthServiceInterface {
  constructor(
    @inject('UsersRepository')
    private repository: UserRepositoryInterface,
  ) { }

  async authenticate({
    email,
    password,
  }: CreateSessionInterface): Promise<AuthResponseInterface> {
    const user = await this.repository.findByEmail(email);
    const passwordMatch = await compare(password, user?.password || '');

    if (!user || !passwordMatch) {
      throw new ErrorNo(400, 'Invalid e-mail or password');
    }

    const token = sign({}, '797baf91d5ffb6525e31d78bf53aa178897538b6', {
      subject: user.id,
      expiresIn: '1d',
    });

    return {
      user: {
        name: user.name,
        email: user.email,
      },
      token,
    };
  }
}

export default AuthService;
