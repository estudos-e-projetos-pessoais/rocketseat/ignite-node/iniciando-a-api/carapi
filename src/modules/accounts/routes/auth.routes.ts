import { Router } from 'express';

import AuthController from '../controllers/AuthController';

const authRouter = Router();

const authController = new AuthController();

authRouter.post('/authenticate',
  authController.authenticate.bind(authController));

export default authRouter;
