import { Router } from 'express';
import multer from 'multer';

import { ensureAuthenticatedMiddleware } from '../../../middlewares/ensureAuthenticatedMiddleware';
import { multerUploadConfig } from '../../../utils/file';
import UsersController from '../controllers/UsersController';

const usersRouter = Router();

const usersController = new UsersController();

const uploadAvatar = multer(multerUploadConfig('./tmp/avatar'));
// const uploadAvatar = multer({ dest: './tmp', });

usersRouter.post('/',
  usersController.create.bind(usersController));

usersRouter.put('/:id',
  ensureAuthenticatedMiddleware,
  uploadAvatar.single('avatar'),
  usersController.update.bind(usersController));

export default usersRouter;
