import User from '../../models/User';

export interface CreateUserDTOInterface {
  name: string;
  email: string;
  password: string
  driver_license: string;
  avatar?: string;
}

export interface UpdateUserDTOInterface {
  id: string;
  name?: string;
  email?: string;
  driver_license?: string;
  avatar?: string;
}

interface UserRepositoryInterface {
  // index(): Promise<User[]>,
  create(data: CreateUserDTOInterface): Promise<User>,
  update(data: UpdateUserDTOInterface): Promise<User>,
  findByEmail(email: string): Promise<User>,
  findById(id: string): Promise<User>,
}

export default UserRepositoryInterface;
