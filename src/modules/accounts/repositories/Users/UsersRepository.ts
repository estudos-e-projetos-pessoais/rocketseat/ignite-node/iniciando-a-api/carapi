import { Repository, getRepository } from 'typeorm';

import User from '../../models/User';
import UserRepositoryInterface, {
  CreateUserDTOInterface,
  UpdateUserDTOInterface,
} from './UsersRepositoryInterface';

class UserRepository implements UserRepositoryInterface {
  private repository: Repository<User>;

  constructor() {
    this.repository = getRepository(User);
  }

  // index(): Promise<User[]> {
  //   return this.repository.find();
  // }

  create({
    name,
    email,
    password,
    driver_license,
  }: CreateUserDTOInterface): Promise<User> {
    const newUser = this.repository.create({
      name,
      email,
      password,
      driver_license,
    });

    return this.repository.save(newUser);
  }

  update(user: UpdateUserDTOInterface): Promise<User> {
    return this.repository.save(user);
  }

  findByEmail(email: string): Promise<User> {
    return this.repository.findOne({ email });
  }

  findById(id: string): Promise<User> {
    return this.repository.findOne({ id });
  }
}

export default UserRepository;
