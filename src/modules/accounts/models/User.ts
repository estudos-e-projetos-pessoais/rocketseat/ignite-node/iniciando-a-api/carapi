import {
  Entity,
  PrimaryColumn,
  Column,
  CreateDateColumn,
  Generated,
} from 'typeorm';
import { v4 as uuid } from 'uuid';

@Entity('users')
class User {
  @PrimaryColumn({ type: 'uuid', unique: true })
  @Generated('uuid')
  id: string;

  @Column()
  name: string;

  @Column()
  password: string;

  @Column()
  email: string;

  @Column('varchar', { nullable: true })
  avatar: string;

  @Column()
  driver_license: string;

  @Column('boolean', { default: false })
  admin: boolean;

  @CreateDateColumn()
  created_at: Date;

  constructor() {
    if (!this.id) {
      this.id = uuid();
    }
  }
}

export default User;
