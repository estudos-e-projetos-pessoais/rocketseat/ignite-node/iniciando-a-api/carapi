import { Response } from 'express';

class ErrorNo extends Error {
  code: number;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  constructor(code: number, ...args: any[]) {
    super(...args);
    this.code = code;
    Object.setPrototypeOf(this, ErrorNo.prototype);
  }
}

function controllerErrorHandler(res: Response, err: ErrorNo): Response {
  console.error('ERROR', err);

  if (!err.code) {
    return res.status(err.code || 500).send('Internal Server Error');
  }

  return res.status(err.code).json({ code: err.code, message: err.message });
}

export {
  ErrorNo,
  controllerErrorHandler,
};
