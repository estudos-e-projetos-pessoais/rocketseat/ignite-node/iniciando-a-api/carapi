import crypto from 'crypto';
import fs from 'fs';
import multer, { StorageEngine } from 'multer';
import { resolve } from 'path';

interface UploadInterface {
  storage: StorageEngine
}

export function multerUploadConfig(folder: string): UploadInterface {
  return {
    storage: multer.diskStorage({
      destination: resolve(__dirname, '..', '..', folder),
      filename: (request, file, callback) => {
        const filehash = crypto.randomBytes(16).toString('hex');
        const filename = `${filehash}-${file.originalname}`;

        return callback(null, filename);
      },
    }),
  };
}

export async function removeFileFromDisk(filename: string): Promise<void> {
  try {
    await fs.promises.unlink(filename);
  }
  // eslint-disable-next-line no-empty
  catch (err) {

  }
}
