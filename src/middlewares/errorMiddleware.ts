import { NextFunction, Request, Response } from 'express';

import { ErrorNo } from '../utils/errorUtils';

export function errorMiddleware(
  err: Error | ErrorNo,
  req: Request,
  res: Response,
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  next: NextFunction, // por algum motivo se essa linha não tiver aqui o app quebra
): Response {
  // eslint-disable-next-line no-console
  console.log(err);

  if (err instanceof ErrorNo) {
    return res.status(err.code).json({
      code: err.code,
      message: err.message,
    });
  }

  return res.status(500).json({
    code: 500,
    message: 'Internal Server Error',
  });
}
