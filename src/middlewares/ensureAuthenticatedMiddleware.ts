import { NextFunction, Request, Response } from 'express';
import { verify } from 'jsonwebtoken';

import UserRepository from '../modules/accounts/repositories/Users/UsersRepository';
import { ErrorNo } from '../utils/errorUtils';

interface JWTPayloadInterface {
  sub: string;
}

export async function ensureAuthenticatedMiddleware(
  req: Request,
  res: Response,
  next: NextFunction,
): Promise<Response | void> {
  const authHeader = req.headers.authorization;

  if (!authHeader) {
    throw new ErrorNo(401, 'User not authenticated');
  }

  try {
    const [, token] = authHeader.split(' ');
    const { sub: user_id } = verify(token, '797baf91d5ffb6525e31d78bf53aa178897538b6') as JWTPayloadInterface;

    const usersRepository = new UserRepository();
    const user = await usersRepository.findById(user_id);

    if (!user) {
      throw new ErrorNo(404, 'User does not exist');
    }

    return next();
  }
  catch (err) {
    throw new ErrorNo(401, 'Invalid Token');
  }
}
