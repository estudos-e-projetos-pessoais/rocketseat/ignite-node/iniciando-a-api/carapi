import 'reflect-metadata';
import express from 'express';
import 'express-async-errors';
import swaggerUi from 'swagger-ui-express';

import swaggerConfig from '../swagger.json';
import './shared/container';
import createConnection from './database';
import { errorMiddleware } from './middlewares/errorMiddleware';

(async () => {
  await createConnection();
  const accountRoutes = (await import('./modules/accounts/routes')).default;
  const carRoutes = (await import('./modules/cars/routes')).default;

  const PORT = 3333;

  const app = express();

  app.use('/api-docs',
    swaggerUi.serve,
    swaggerUi.setup(swaggerConfig));

  app.use(express.json());
  app.use(express.urlencoded({ extended: true }));

  app.use(carRoutes);
  app.use(accountRoutes);

  app.use(errorMiddleware);

  app.listen(PORT, () => {
  // eslint-disable-next-line no-console
    console.log(`App is running on port ${PORT}`);
  });
})();
