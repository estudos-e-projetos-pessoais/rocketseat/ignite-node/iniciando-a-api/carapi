import { container } from 'tsyringe';

import UsersRepository from '../../modules/accounts/repositories/Users/UsersRepository';
import UsersRepositoryInterface from '../../modules/accounts/repositories/Users/UsersRepositoryInterface';
import CategoriesRepository from '../../modules/cars/repositores/Categories/CategoriesRepository';
import CategoriesRepositoryInterface from '../../modules/cars/repositores/Categories/CategoriesRepositoryInterface';
import SpecificationsRepository from '../../modules/cars/repositores/Specifications/SpecificationsRepository';
import SpecificationsRepositoryInterface from '../../modules/cars/repositores/Specifications/SpecificationsRepositoryInterface';

container.registerSingleton<CategoriesRepositoryInterface>(
  'CategoriesRepository',
  CategoriesRepository,
);

container.registerSingleton<SpecificationsRepositoryInterface>(
  'SpecificationsRepository',
  SpecificationsRepository,
);

container.registerSingleton<UsersRepositoryInterface>(
  'UsersRepository',
  UsersRepository,
);
