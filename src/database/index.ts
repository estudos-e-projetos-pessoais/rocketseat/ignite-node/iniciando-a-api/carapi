import { createConnection, Connection } from 'typeorm';

function _createConnection(): Promise<Connection> {
  return createConnection();
}

export default _createConnection;
